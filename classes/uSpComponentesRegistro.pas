unit uSpComponentesRegistro;

interface
   uses
      System.Classes, uSpQuery;

   procedure Register;

implementation

procedure Register;
begin
   System.Classes.RegisterComponents('spComponentes', [TSpQuery]);
end;

end.
