unit uThdProgressA;

interface

uses
  System.Classes, Vcl.ComCtrls, Vcl.StdCtrls, System.SysUtils;

type
  TThdProgressA = class(TThread)
  private
    { Private declarations }
    FProgressBar: TProgressBar;
    FLabelCount: TLabel;
    FIntervalo: Integer;

  public
     constructor Create(pProgressBar: TProgressBar; pLabelCount: TLabel; const pIntervalo: integer);
  protected
     procedure Execute; override;
  end;

implementation


{ TThdProgressA }

constructor TThdProgressA.Create(pProgressBar: TProgressBar; pLabelCount: TLabel; const pIntervalo: integer);
begin
   inherited Create(False);
   FreeOnTerminate := True;
   FProgressBar    := pProgressBar;
   FIntervalo      := pIntervalo;
   FLabelCount     := pLabelCount;
end;

procedure TThdProgressA.Execute;
var
   lContador: Integer;
begin
   for lContador := 0 to 100 do
   begin
      Sleep(FIntervalo);
      Synchronize(Self,
         procedure ()
         begin
            FLabelCount.Caption := lContador.ToString;
            FProgressBar.StepIt;
         end
      );
   end;
end;

end.
