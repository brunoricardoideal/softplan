unit uSpQuery;

interface
   uses
      Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client, System.Classes, System.SysUtils,
      System.StrUtils, Firedac.Dapt, FireDAC.Stan.Intf,
      FireDAC.Phys, FireDAC.Phys.TDBXBase;

type
   TSpQuery = class(TFDQuery)
      private
         var
            FspCondicoes : TStringList;
            FspColunas   : TStringList;
            FspTabelas   : TStringList;

         function GetSQLTabelas: string;
         function GetSQLColunas: string;
         function GetSQLCondicoes: string;
      public
         constructor Create(pOwner: TComponent);override;
         destructor  Destroy;override;

         procedure GeraSQL;

      protected

      published
         property spCondicoes: TStringList read FspCondicoes write FspCondicoes;
         property spColunas: TStringList read FspColunas write FspCondicoes;
         property spTabelas: TStringList read FspTabelas write FspTabelas;
   end;

implementation

   uses
      uConstantesTarefa;


{ TSpQuery }

constructor TSpQuery.Create(pOwner: TComponent);
begin
   inherited;
   FspCondicoes := TStringList.Create;
   FspColunas   := TStringList.Create;
   FspTabelas   := TStringList.Create;
end;

destructor TSpQuery.Destroy;
begin
   if Assigned(FspCondicoes) then
   begin
      FspCondicoes.Destroy;
   end;

   if Assigned(FspColunas) then
   begin
      FspColunas.Destroy;
   end;

   if Assigned(FspTabelas) then
   begin
      FspTabelas.Destroy;
   end;

   inherited;
end;

procedure TSpQuery.GeraSQL;
begin
   SQL.Clear;
   SQL.Add('SELECT ' + GetSQLColunas);
   SQL.Add('FROM '   + GetSQLTabelas);
   SQL.Add('WHERE '   + GetSQLCondicoes);
end;

function TSpQuery.GetSQLColunas: string;
const
   L_SUFIXO_MSG_COLUNA = 'uma coluna';
var
   lCampo, lSQL: String;
   lStrIntermediaria: String;
begin
   Result := '';

   if FspColunas.Text.Trim.IsEmpty then
   begin
      raise Exception.Create(Format(MSG_SEM_OBJETO, [L_SUFIXO_MSG_COLUNA]));
   end;

   lSQL := EmptyStr;
   for lCampo in FspColunas do
   begin
      lStrIntermediaria := sLineBreak + IfThen(lSQL.IsEmpty, ' ', ' ,');
      lSQL := lSQL + lStrIntermediaria + lCampo;
   end;

   Result := lSQL;
end;

function TSpQuery.GetSQLCondicoes: string;
var
   lCondicao, lSQL: string;
begin
   Result := '';

   lSQL := ' 1 = 1 ' + sLineBreak;
   for lCondicao in FspCondicoes do
   begin
      lSQL := sLineBreak + lSQL + ' and ' + lCondicao;
   end;

   Result := lSQL;
end;

function TSpQuery.GetSQLTabelas: string;
const
   L_SUFIXO_MSG_TABELA = 'uma tabela';
begin
   Result := '';

   if FspTabelas.Text.Trim.IsEmpty then
   begin
      raise Exception.Create(Format(MSG_SEM_OBJETO, [L_SUFIXO_MSG_TABELA]));
   end;
   if FspTabelas.Count > 1 then

   begin
      raise Exception.Create(MSG_APENAS_UMA_TABELA);
   end;

   Result := FspTabelas.Text.Trim;
end;

end.
