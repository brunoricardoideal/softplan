unit uProjetoModel;

interface

type
   TProjetoModel = class(TObject)
      private
         Fvalor       : Double;
         FidProjeto   : Integer;
         FnomeProjeto : string;

      public
         constructor Create;overload;
         constructor Creaate(const pId: integer; const pNome: string; const pValor: Double);overload;

         property idProjeto: Integer read FidProjeto write FidProjeto;
         property nomeProjeto: string read FnomeProjeto write FnomeProjeto;
         property valor: Double read Fvalor write Fvalor;
   end;

implementation

{ TProjetoModel }

constructor TProjetoModel.Creaate(const pId: integer; const pNome: string; const pValor: Double);
begin
   inherited Create;
   FidProjeto   := pId;
   FnomeProjeto := pNome;
   Fvalor       := pValor;
end;

constructor TProjetoModel.Create;
begin
   FidProjeto   := 0;
   FnomeProjeto := '';
   Fvalor       := 0.00;
end;

end.
