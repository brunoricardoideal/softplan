unit ufTarefa2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFMaster, Vcl.StdCtrls, Vcl.ComCtrls, uTarefa2Controller, Vcl.Samples.Spin;

type
  TfTarefa2 = class(TfMaster)
    pgA: TProgressBar;
    pgB: TProgressBar;
    btnIniciar: TButton;
    edtTempoA: TSpinEdit;
    edtTempoB: TSpinEdit;
    lblPgA: TLabel;
    lblPgB: TLabel;
    procedure btnIniciarClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
     procedure Iniciar;
     procedure InitComp;
  public
    { Public declarations }
  protected
     procedure CriarController;override;
  end;

var
  fTarefa2: TfTarefa2;

implementation

{$R *.dfm}

{ TfTarefa2 }

procedure TfTarefa2.btnIniciarClick(Sender: TObject);
begin
   inherited;
   if not TTarefa2Controller(FController).Terminated then
   begin
      ShowMessage('Processo j� em andamento, aguarde.');
      Exit;
   end;

   Iniciar;
end;

procedure TfTarefa2.CriarController;
begin
   inherited;
   FController := TTarefa2Controller.Create;
end;

procedure TfTarefa2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   inherited;
   fTarefa2 := nil;
end;

procedure TfTarefa2.Iniciar;
begin
   InitComp;
   TTarefa2Controller(FController).SetLblA(lblPgA).IniciarProgressA(pgA, edtTempoA.Value);
   TTarefa2Controller(FController).IniciarAnonProgressB(pgB, lblPgB, edtTempoB.Value);
end;

procedure TfTarefa2.InitComp;
begin
   //Progressbars
   pgA.Position := 0;
   pgB.Position := 0;

   //Labels
   lblPgA.Caption := pgA.Position.ToString;
   lblPgB.Caption := pgB.Position.ToString;
end;

end.
