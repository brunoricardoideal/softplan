unit ufTarefa1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param, 
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf, FireDAC.Stan.Async, FireDAC.DApt, Data.DB,
  FireDAC.Comp.DataSet, FireDAC.Comp.Client, uSpQuery, uFMaster, uTarefa1Controller;

type
  TfTarefa1 = class(TfMaster)
    lblColunas: TLabel;
    memoColunas: TMemo;
    lblTabelas: TLabel;
    memoTabelas: TMemo;
    lblCondicoes: TLabel;
    memoCondicoes: TMemo;
    memoSQlGerado: TMemo;
    lblSQLGerado: TLabel;
    btnGeraSQL: TButton;
    procedure btnGeraSQLClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }

     procedure GerarSQL;
  public
    { Public declarations }
  protected
     procedure CriarController;override;
  end;

var
  fTarefa1: TfTarefa1;

implementation

{$R *.dfm}

{ TfTarefa1 }

procedure TfTarefa1.btnGeraSQLClick(Sender: TObject);
begin
   memoSQlGerado.Clear;
   GerarSQL;
end;

procedure TfTarefa1.CriarController;
begin
   inherited;
   FController := TTarefa1Controller.Create;
end;

procedure TfTarefa1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   inherited;
   fTarefa1 := nil;
end;

procedure TfTarefa1.GerarSQL;
begin
   memoSQlGerado.Text := TTarefa1Controller(FController).GeraSQL(memoColunas.Text, memoTabelas.Text, memoCondicoes.Text);
end;

end.
