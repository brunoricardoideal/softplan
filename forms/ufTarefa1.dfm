object fTarefa1: TfTarefa1
  Left = 617
  Top = 309
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Tarefa 1'
  ClientHeight = 333
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lblColunas: TLabel
    Left = 32
    Top = 24
    Width = 38
    Height = 13
    Caption = 'Colunas'
  end
  object lblTabelas: TLabel
    Left = 127
    Top = 24
    Width = 37
    Height = 13
    Caption = 'Tabelas'
  end
  object lblCondicoes: TLabel
    Left = 222
    Top = 24
    Width = 49
    Height = 13
    Caption = 'Condi'#231#245'es'
  end
  object lblSQLGerado: TLabel
    Left = 32
    Top = 160
    Width = 56
    Height = 13
    Caption = 'SQL gerado'
  end
  object memoColunas: TMemo
    Left = 32
    Top = 43
    Width = 89
    Height = 89
    TabOrder = 0
  end
  object memoTabelas: TMemo
    Left = 127
    Top = 43
    Width = 89
    Height = 89
    TabOrder = 1
  end
  object memoCondicoes: TMemo
    Left = 222
    Top = 43
    Width = 163
    Height = 89
    TabOrder = 2
  end
  object memoSQlGerado: TMemo
    Left = 32
    Top = 179
    Width = 353
    Height = 126
    ReadOnly = True
    TabOrder = 3
  end
  object btnGeraSQL: TButton
    Left = 408
    Top = 80
    Width = 75
    Height = 25
    Caption = 'GeraSQL'
    TabOrder = 4
    OnClick = btnGeraSQLClick
  end
end
