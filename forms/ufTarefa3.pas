unit ufTarefa3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, uFMaster, uTarefa3Controller, Vcl.StdCtrls, Data.DB, Vcl.Grids,
  Vcl.DBGrids, System.Actions, Vcl.ActnList, Vcl.ExtCtrls;

type
  TfTarefa3 = class(TfMaster)
    lblTituloGrdProjetos: TLabel;
    grdProjetos: TDBGrid;
    edtTotal: TLabeledEdit;
    edtTotalDivisoes: TLabeledEdit;
    btnObterTotal: TButton;
    acLstTarefa3: TActionList;
    acObterTotal: TAction;
    acObterTotalDivisoes: TAction;
    btnName: TButton;
    procedure acObterTotalExecute(Sender: TObject);
    procedure acObterTotalDivisoesExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
      function GetControllerTarefa3: TTarefa3Controller;
  public
    { Public declarations }
  protected
     procedure CriarController;override;
  end;

var
  fTarefa3: TfTarefa3;

implementation

uses
  System.Math, uConstantesTarefa;

{$R *.dfm}

{ TfTarefa3 }

procedure TfTarefa3.acObterTotalDivisoesExecute(Sender: TObject);
begin
  inherited;
  edtTotalDivisoes.Text := 'R$ ' + FormatCurr(FORMATO_CURRENCY, GetControllerTarefa3.GetTotalDivisoes);
end;

procedure TfTarefa3.acObterTotalExecute(Sender: TObject);
begin
   inherited;
   edtTotal.Text := 'R$ ' + FormatCurr(FORMATO_CURRENCY, GetControllerTarefa3.GetTotal);
end;

procedure TfTarefa3.CriarController;
begin
   inherited;
   FController := TTarefa3Controller.Create;
end;

procedure TfTarefa3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   inherited;
   fTarefa3 := nil;
end;

procedure TfTarefa3.FormCreate(Sender: TObject);
begin
   inherited;
   edtTotal.Text          := 'R$ 0.00';
   edtTotalDivisoes.Text  := 'R$ 0.00';
   grdProjetos.DataSource := GetControllerTarefa3.Ds;
end;

procedure TfTarefa3.FormShow(Sender: TObject);
begin
   inherited;
   GetControllerTarefa3.SetGrid(grdProjetos);
   GetControllerTarefa3.Popular;
end;

function TfTarefa3.GetControllerTarefa3: TTarefa3Controller;
begin
   Result := TTarefa3Controller(FController);
end;

end.
