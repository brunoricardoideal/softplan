inherited fTarefa3: TfTarefa3
  BorderStyle = bsSingle
  Caption = 'Tarefa 3'
  ClientHeight = 309
  ClientWidth = 645
  FormStyle = fsMDIChild
  Visible = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblTituloGrdProjetos: TLabel
    Left = 32
    Top = 24
    Width = 92
    Height = 13
    Caption = 'Valores por projeto'
  end
  object grdProjetos: TDBGrid
    Left = 32
    Top = 43
    Width = 585
    Height = 150
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        Title.Caption = 'idProjeto'
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 'NomeProjeto'
        Width = 363
        Visible = True
      end
      item
        Expanded = False
        Title.Caption = 'Valor R$'
        Width = 75
        Visible = True
      end>
  end
  object edtTotal: TLabeledEdit
    Left = 496
    Top = 216
    Width = 121
    Height = 21
    EditLabel.Width = 44
    EditLabel.Height = 13
    EditLabel.Caption = 'Total R$:'
    ReadOnly = True
    TabOrder = 1
  end
  object edtTotalDivisoes: TLabeledEdit
    Left = 496
    Top = 256
    Width = 121
    Height = 21
    EditLabel.Width = 85
    EditLabel.Height = 13
    EditLabel.Caption = 'Total divis'#245'es R$:'
    ReadOnly = True
    TabOrder = 2
  end
  object btnObterTotal: TButton
    Left = 415
    Top = 214
    Width = 75
    Height = 25
    Action = acObterTotal
    TabOrder = 3
  end
  object btnName: TButton
    Left = 376
    Top = 254
    Width = 114
    Height = 25
    Action = acObterTotalDivisoes
    TabOrder = 4
  end
  object acLstTarefa3: TActionList
    Left = 384
    Top = 120
    object acObterTotal: TAction
      Caption = 'Obter Total'
      OnExecute = acObterTotalExecute
    end
    object acObterTotalDivisoes: TAction
      Caption = 'Obter Total Divisoes'
      OnExecute = acObterTotalDivisoesExecute
    end
  end
end
