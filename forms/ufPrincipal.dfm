object FrmPrincipal: TFrmPrincipal
  Left = 0
  Top = 0
  Caption = 'Principal'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = mmPrincipal
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object mmPrincipal: TMainMenu
    Left = 168
    Top = 104
    object mmPrincItemTarefas: TMenuItem
      Caption = 'Tarefas'
      object mmPrincItemTarefa1: TMenuItem
        Action = acChamaTarefa1
      end
      object mmPrincItemTarefa2: TMenuItem
        Action = acChamaTarefa2
      end
      object mmPrincItemTarefa3: TMenuItem
        Action = acChamaTarefa3
      end
    end
  end
  object acLstPrincipal: TActionList
    Left = 168
    Top = 56
    object acChamaTarefa1: TAction
      Caption = 'Tarefa1'
      OnExecute = acChamaTarefa1Execute
    end
    object acChamaTarefa2: TAction
      Caption = 'Tarefa2'
      OnExecute = acChamaTarefa2Execute
    end
    object acChamaTarefa3: TAction
      Caption = 'Tarefa3'
      OnExecute = acChamaTarefa3Execute
    end
  end
end
