unit ufPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, System.Actions, Vcl.ActnList, Vcl.Menus, uFMaster, uPrincipalController;

type
  TFrmPrincipal = class(TFMaster)
    mmPrincipal: TMainMenu;
    mmPrincItemTarefas: TMenuItem;
    mmPrincItemTarefa1: TMenuItem;
    mmPrincItemTarefa2: TMenuItem;
    mmPrincItemTarefa3: TMenuItem;
    acLstPrincipal: TActionList;
    acChamaTarefa1: TAction;
    acChamaTarefa2: TAction;
    acChamaTarefa3: TAction;
    procedure acChamaTarefa1Execute(Sender: TObject);
    procedure acChamaTarefa2Execute(Sender: TObject);
    procedure acChamaTarefa3Execute(Sender: TObject);
  private
     function GetPrincController: TPrincipalController;
    { Private declarations }
  public
    { Public declarations }
  protected
     procedure CriarController;override;
  end;

var
  FrmPrincipal: TFrmPrincipal;

implementation

{$R *.dfm}

{ TFrmPrincipal }

procedure TFrmPrincipal.acChamaTarefa1Execute(Sender: TObject);
begin
   GetPrincController.ChamarFormTarefa(tf01);
end;

procedure TFrmPrincipal.acChamaTarefa2Execute(Sender: TObject);
begin
   inherited;
   GetPrincController.ChamarFormTarefa(tf02);
end;

procedure TFrmPrincipal.acChamaTarefa3Execute(Sender: TObject);
begin
  inherited;
   GetPrincController.ChamarFormTarefa(tf03);
end;

procedure TFrmPrincipal.CriarController;
begin
   inherited;
   FController := TPrincipalController.Create(Self);
end;

function TFrmPrincipal.GetPrincController: TPrincipalController;
begin
   Result := TPrincipalController(FController);
end;

end.
