inherited fTarefa2: TfTarefa2
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'Tarefa 2'
  ClientHeight = 145
  ClientWidth = 511
  FormStyle = fsMDIChild
  Visible = True
  ExplicitWidth = 517
  ExplicitHeight = 174
  PixelsPerInch = 96
  TextHeight = 13
  object lblPgA: TLabel
    Left = 208
    Top = 32
    Width = 6
    Height = 13
    Caption = '0'
  end
  object lblPgB: TLabel
    Left = 208
    Top = 87
    Width = 6
    Height = 13
    Caption = '0'
  end
  object pgA: TProgressBar
    Left = 40
    Top = 9
    Width = 385
    Height = 17
    Step = 1
    TabOrder = 0
  end
  object pgB: TProgressBar
    Left = 40
    Top = 64
    Width = 385
    Height = 17
    Step = 1
    TabOrder = 1
  end
  object btnIniciar: TButton
    Left = 421
    Top = 104
    Width = 75
    Height = 25
    Caption = 'Iniciar'
    TabOrder = 2
    OnClick = btnIniciarClick
  end
  object edtTempoA: TSpinEdit
    Left = 431
    Top = 9
    Width = 65
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 3
    Value = 500
  end
  object edtTempoB: TSpinEdit
    Left = 431
    Top = 64
    Width = 65
    Height = 22
    MaxValue = 5000
    MinValue = 1
    TabOrder = 4
    Value = 350
  end
end
