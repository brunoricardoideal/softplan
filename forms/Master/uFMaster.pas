unit uFMaster;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs;

type
  TfMaster = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }

  public
    { Public declarations }
  protected
     FController: TObject;
     procedure CriarController;virtual;abstract;

  end;

var
  fMaster: TfMaster;

implementation

{$R *.dfm}

procedure TfMaster.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   Action := caFree;
end;

procedure TfMaster.FormCreate(Sender: TObject);
begin
   CriarController;
end;

procedure TfMaster.FormDestroy(Sender: TObject);
begin
   if Assigned(FController) then
   begin
      FController.Destroy;
   end;
end;

end.
