unit uTarefa2Controller;

interface

uses
   uThdProgressA, Vcl.ComCtrls, System.Classes, Vcl.StdCtrls;

type
   TTarefa2Controller = class(TObject)
      private
         FThdA: TThdProgressA;
         FThdB: TThread;
         FLblCountA, FLblCountB: TLabel;
      public
         destructor Destroy;override;

         procedure IniciarProgressA(pProgressA: TProgressBar; pIntervalo: Integer);
         procedure IniciarAnonProgressB(pProgressB: TProgressBar; pLblB: TLabel; pIntervalo: Integer);
         procedure TerminateThd;
         function SetLblA(pLabel: TLabel): TTarefa2Controller;
         function Terminated: boolean;
   end;

implementation

uses
  System.SysUtils, Winapi.Windows;

{ TTarefa2Controller }

destructor TTarefa2Controller.Destroy;
begin
   TerminateThd;
   inherited;
end;

procedure TTarefa2Controller.IniciarAnonProgressB(pProgressB: TProgressBar; pLblB: TLabel; pIntervalo: Integer);
var
   lThd: TThread;
begin
   FLblCountB := pLblB;

   if Assigned(FThdB) then
   begin
      FThdB.Terminate;
   end;

   lThd := TThread.CreateAnonymousThread(
      procedure()
         var
            lContador: Integer;
      begin
         for lContador := 0 to 100 do
         begin
            Sleep(pIntervalo);
            TThread.Synchronize(lThd,
               procedure()
               begin
                  if Assigned(FLblCountB) then
                  begin
                     FLblCountB.Caption := IntToStr(lContador);
                  end;

                  if Assigned(pProgressB) then
                  begin
                     pProgressB.StepIt;
                  end;
               end
            );
         end;
      end
   );
   lThd.Start;
end;

procedure TTarefa2Controller.IniciarProgressA(pProgressA: TProgressBar; pIntervalo: Integer);
begin
   if Assigned(FThdA) and (FThdA.Started) then
   begin
      FThdA.Terminate;
   end;

   FThdA := TThdProgressA.Create(pProgressA, FLblCountA, pIntervalo);
end;

function TTarefa2Controller.SetLblA(pLabel: TLabel): TTarefa2Controller;
begin
   FLblCountA := pLabel;

   Result := Self;
end;

function TTarefa2Controller.Terminated: boolean;
begin
   Result := ((Assigned(FThdA) and (not FThdA.Started)) or (not Assigned(FThdA))) and
             ((Assigned(FThdB) and (not FThdB.Started)) or (not Assigned(FThdB)));

end;

procedure TTarefa2Controller.TerminateThd;
begin
   if Assigned(FThdA) then
   begin
      TerminateThread(FThdA.Handle, 10);
      FreeAndNil(FThdA);
   end;

   if Assigned(FThdB) then
   begin
      TerminateThread(FThdB.Handle, 11);
      FreeAndNil(FThdB);
   end;
end;

end.
