unit uPrincipalController;

interface

   uses
      ufTarefa1, System.Classes, System.Sysutils, Vcl.Forms, ufTarefa2, ufTarefa3;

type
   TNumTarefa = (tf01, tf02, tf03);

   TPrincipalController = class(TObject)
      private
         FOwner: TComponent;
      public
         constructor Create(pOwner: TComponent);
         procedure ChamarFormTarefa(const pTarefa: TNumTarefa);
   end;

implementation

{ TPrincipalController }

procedure TPrincipalController.ChamarFormTarefa(const pTarefa: TNumTarefa);
begin
   case pTarefa of
      tf01:
      begin
         if not Assigned(fTarefa1) then
         begin
            Application.CreateForm(TfTarefa1, fTarefa1);
         end;
         fTarefa1.Show;
      end;
      tf02:
      begin
         if not Assigned(fTarefa2) then
         begin
            Application.CreateForm(TfTarefa2, fTarefa2);
         end;
         fTarefa2.Show;
      end;
      tf03:
      begin
         if not Assigned(fTarefa3) then
         begin
            Application.CreateForm(TfTarefa3, fTarefa3);
         end;
         fTarefa3.Show;
      end;
   end;
end;

constructor TPrincipalController.Create(pOwner: TComponent);
begin
   inherited Create;
   FOwner := pOwner;
end;

end.
