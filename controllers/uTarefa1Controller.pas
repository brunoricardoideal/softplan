unit uTarefa1Controller;

interface
   uses
      uspQuery;

type
   TTarefa1Controller = class(TObject)
      private
         FspQuery: TSpQuery;

      public
         constructor Create;
         destructor Destroy;override;

         function GeraSQL(const pColunas, pTabelas, pCondicoes: string): string;

         property spQuery: TSpQuery read FspQuery;
   end;

implementation

{ TTarefa1Controller }



{ TTarefa1Controller }

constructor TTarefa1Controller.Create;
begin
   inherited Create;
   FspQuery := TSpQuery.Create(nil);
end;

destructor TTarefa1Controller.Destroy;
begin
   FspQuery.Destroy;
   inherited;
end;

function TTarefa1Controller.GeraSQL(const pColunas, pTabelas, pCondicoes: string): string;
begin
   FspQuery.SQL.Clear;
   FspQuery.spColunas.Text   := pColunas;
   FspQuery.spTabelas.Text   := pTabelas;
   FspQuery.spCondicoes.Text := pCondicoes;
   FspQuery.GeraSQL;
   Result := FspQuery.SQL.Text;
end;

end.
