unit uTarefa3Controller;

interface
   uses
      DbClient, System.SysUtils, System.Rtti, System.Classes, uProjetoModel,
      Data.DB, Generics.Collections, Vcl.DBGrids, Math, MidasLib;

type
   TTarefa3Controller = class
      private
         const
            CAMPO_ID = 'idProjeto';

         var
            FCds: TClientDataSet;
            FDs: TDataSource;
            FDic: TObjectDictionary<integer, TProjetoModel>;

         /// <summary> Cria os TFields do TclientDataSet mediante modelo estabelecido
         /// </summary>
         /// <param name="pProjetoModel">Model do projeto
         /// </param>
         /// <remarks> Aplica as t�cnicas de rtti no ato da configura��o
         /// </remarks>
         procedure CriarTodosFields(const pProjetoModel: TProjetoModel);
      public
         constructor Create;
         destructor Destroy;override;

         /// <summary> Gera projetos aleatorios e popula o dicion�rio e o TClientDataSet respons�vel em ligar-se na view
         /// </summary>
         procedure Popular;

         /// <summary> Seta a grid respons�vel por exibi��o assim como a configura
         /// </summary>
         /// <param name="pDbGrid">TDBGrid pertencente � view
         /// </param>
         procedure SetGrid(pDbGrid: TDBGrid);

         /// <summary> Cria um record no TclientDataSet representando o respectivo model
         /// </summary>
         /// <param name="pProjetoModel">Model do projeto
         /// </param>
         /// <remarks> Aplica as t�cnicas de rtti no ato do preenchimento
         /// </remarks>
         procedure CriarRegistroCds(const pProjetoModel: TProjetoModel);

         /// <summary> Itera os itens do dicion�rio e retorna o valor total
         /// </summary>
         /// <returns>Valor total dos projetos
         /// </returns>
         function GetTotal: Double;

         /// <summary> Itera os itens do TclientDataset e realizae as divis�e
         /// </summary>
         /// <returns>Valor total das divis�es
         /// </returns>
         /// <remarks> "(...)total das divis�es do registro seguinte pelo registro anterior(...)"
         /// </remarks>
         function GetTotalDivisoes: Double;

         /// <summary>Obtem o projeto de c�digo desejado
         /// </summary>
         /// <returns>Modelo do projeto
         /// </returns>
         function GetProjeto(const Id: Integer): TProjetoModel;

         property Cds: TClientDataSet read FCds write FCds;
         property Ds: TDataSource read FDs write FDs;
         property Dic: TObjectDictionary<integer, TProjetoModel> read FDic write FDic;
   end;

implementation

   uses
      uConstantesTarefa, Winapi.ActiveX, System.TypInfo, Vcl.Forms;

{ TTarefa3Controller }

constructor TTarefa3Controller.Create;
var
   lModel: TProjetoModel;
begin
   inherited Create;
   FCds := TClientDataSet.Create(Application);
   FDs  := TDataSource.Create(Application);
   FDs.DataSet := FCds;

   lModel := TProjetoModel.Create;
   try
      CriarTodosFields(lModel);
   finally
      lModel.Free;
   end;

   FDic := TObjectDictionary<integer, TProjetoModel>.Create([doOwnsValues]);
end;

procedure TTarefa3Controller.CriarRegistroCds(const pProjetoModel: TProjetoModel);
var
  lContexto    : TRttiContext;
  lTipo        : TRttiType;
  lPropriedade : TRttiProperty;
  lValue       : TValue;
begin
   FCds.Append;
   lContexto := TRttiContext.Create;
   try
      lTipo := lContexto.GetType(pProjetoModel.ClassInfo);

      if not Assigned(lTipo) then
      begin
         FCds.Cancel;
         Exit;
      end;

      for lPropriedade in lTipo.GetProperties do
      begin
         lValue := lPropriedade.GetValue(pProjetoModel);
         case lValue.Kind of
            tkInteger: FCds.FieldByName(lPropriedade.name).AsInteger := lValue.AsInteger;
            tkFloat:   FCds.FieldByName(lPropriedade.name).AsExtended := lValue.AsExtended;
            tkString, tkUString:  FCds.FieldByName(lPropriedade.name).AsString := lValue.AsVariant;
         end;
      end;
   finally
      lContexto.Free;
   end;
   FCds.Post;
end;

procedure TTarefa3Controller.CriarTodosFields(const pProjetoModel: TProjetoModel);
var
  lContexto    : TRttiContext;
  lTipo        : TRttiType;
  lPropriedade : TRttiProperty;
  lValue       : TValue;
  lField       : TField;
  //anotation pode ser utilizado ainda para definir os tipos
begin
   FCds.FieldDefs.Clear;
   FCds.Fields.Clear;
   lContexto := TRttiContext.Create;
   try
      lTipo := lContexto.GetType(pProjetoModel.ClassInfo);

      if not Assigned(lTipo) then
      begin
         Exit;
      end;

      for lPropriedade in lTipo.GetProperties do
      begin
         lValue := lPropriedade.GetValue(pProjetoModel);
         case lValue.Kind of
            tkInteger: FCds.FieldDefs.Add(lPropriedade.Name, TFieldType.ftInteger);
            tkFloat:   FCds.FieldDefs.Add(lPropriedade.Name, TFieldType.ftFloat);
            tkString, tkUString:  FCds.FieldDefs.Add(lPropriedade.Name, TFieldType.ftString, 150);
            else raise Exception.Create('Tipo inesperado!');
         end;
      end;
   finally
      lContexto.Free;
   end;

   FCds.CreateDataSet;

   for lField in FCds.Fields do
   begin
      if lField is TFloatField then
      begin
         TFloatField(lField).DisplayFormat := FORMATO_CURRENCY;
      end;
   end;

end;

destructor TTarefa3Controller.Destroy;
begin
   FreeAndNil(FCds);
   FDic.Destroy;
   FDs.DisposeOf; //Mais difundido no Firemonkey
   inherited;
end;

function TTarefa3Controller.GetProjeto(const Id: Integer): TProjetoModel;
begin
   Result := FDic[Id];
end;

function TTarefa3Controller.GetTotal: Double;
var
   lId: Integer;
begin
   Result := 0.00;
   for lId in FDic.Keys do
   begin
      Result := Result + FDic[lId].valor;
   end;
   Result := RoundTo(Result, -2);
end;

function TTarefa3Controller.GetTotalDivisoes: Double;
var
   lAnterior, lProximo: TProjetoModel;
begin
   Result := 0.00;

   //Seguir ordena��o da view, nao utilizando somente a do dicion�rio
   FCds.DisableControls;
   try

      FCds.First;
      lAnterior := GetProjeto(FCds.FieldByName(CAMPO_ID).AsInteger);

      FCds.Next;
      lProximo := GetProjeto(FCds.FieldByName(CAMPO_ID).AsInteger);

      while not FCds.Eof do
      begin
         Result    := Result + (lProximo.valor / lAnterior.valor);
         lAnterior := lProximo;

         FCds.Next;
         lProximo  := GetProjeto(FCds.FieldByName(CAMPO_ID).AsInteger);
      end;
   finally
      FCds.EnableControls;
   end;
end;

procedure TTarefa3Controller.Popular;
var
   lContador  : integer;
   lRandInt   : Integer;
   lRandFloat : Double;
   lProjeto   : TProjetoModel;
begin
   FDic.Clear;
   for lContador := 1 to 10 do
   begin
      repeat
         lRandInt   := Trunc(Random(100));
      until not FDic.ContainsKey(lRandInt);

      lRandFloat := Random(50000);
      lProjeto   := TProjetoModel.Creaate(lRandInt, 'Projeto ' + lContador.ToString, lRandFloat);
      FDic.Add(lProjeto.idProjeto, lProjeto);
      CriarRegistroCds(lProjeto);
   end;
end;


procedure TTarefa3Controller.SetGrid(pDbGrid: TDBGrid);
var
   lIterador: Integer;
begin
   pDbGrid.DataSource := FDs;
   for lIterador := 0 to 2 do
   begin
      pDbGrid.Columns[lIterador].FieldName := FCds.Fields[lIterador].FieldName;
   end;
end;

end.
