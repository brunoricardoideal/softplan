unit uTestesUnit;

interface
uses
  DUnitX.TestFramework, uTarefa1Controller, uTarefa3Controller, uProjetoModel,
  Math;

type
  [TestFixture]
  TMyTestObject = class(TObject) 
  strict private
    var
       FTarefa3Controller: TTarefa3Controller;

    procedure AddProjetoTeste3(const pValor: Double);
  public

    [Setup]
    procedure Setup;

    //TAREFA 01
    [Test]
    procedure TestSQLTarefa1SQLCompleto;

    [Test]
    [TestCase('ErroSemTabela', 'coluna1, , coluna2 = 10')]
    [TestCase('ErroSemColuna', ' , tabela1 , coluna2 = 10')]
    procedure TesteExceptionTarefa1(const pColunas, pTabelas, pCondicoes: string); //A aplica��o deve avisar que h� algo em falta

    //TAREFA 03
    [Test]
    [TestCase('SomaA', '100, 300, 20.50, 0')]
    [TestCase('SomaB', '150.37, 155.90, 70.553, 0')]
    [TestCase('DivisaoA', '10, 20, 30, 1')]
    [TestCase('DivisaoB', '30, 40.50, 80.42, 1')]
    procedure TesteTotalSomaDivisao(const pValor1, pValor2, pValor3: Double; pTipo: Integer);
  end;

implementation

uses
  System.SysUtils;

procedure TMyTestObject.AddProjetoTeste3(const pValor: Double);
var
   lModel: TProjetoModel;
   lRand: Integer;
begin
   repeat
      lRand := Random(1000);
   until not FTarefa3Controller.Dic.ContainsKey(lRand);

   lModel := TProjetoModel.Creaate(lRand, 'proj' + lRand.ToString, pValor);
   FTarefa3Controller.Dic.Add(lRand, lModel);
   FTarefa3Controller.CriarRegistroCds(lModel);
end;

procedure TMyTestObject.Setup;
begin
   FTarefa3Controller := TTarefa3Controller.Create;
end;

procedure TMyTestObject.TesteExceptionTarefa1(const pColunas, pTabelas, pCondicoes: string);
var
   lController: TTarefa1Controller;
   lDeuErro: Boolean;
begin
   lDeuErro := False;
   lController := TTarefa1Controller.Create;
   try
      try
         lController.GeraSQL(pColunas, pTabelas, pCondicoes);
      except
         on e: Exception do
         begin
            lDeuErro := True;
         end;
      end;
   finally
      lController.Destroy;
   end;


   if not lDeuErro then
   begin
      raise Exception.Create('Deveria dar exception');
   end;
end;

procedure TMyTestObject.TesteTotalSomaDivisao(const pValor1, pValor2, pValor3: Double; pTipo: Integer);
var
   lEsperado, lObtido: Double;
begin
   lEsperado := 0.00;
   lObtido   := 0.00;

   FTarefa3Controller.Dic.Clear;
   AddProjetoTeste3(pValor1);
   AddProjetoTeste3(pValor2);
   AddProjetoTeste3(pValor3);

   case pTipo of
      0://Soma
      begin
         lObtido   := FTarefa3Controller.GetTotal;
         lEsperado := RoundTo((pValor1 + pValor2 + pValor3), -2);
      end;
      1://Divisao
      begin
         if (pValor2 = 0) or (pValor3 = 0) then
         begin
            raise Exception.Create('Valores 2 e 3 devem ser positivos');
         end;

         lEsperado := pValor2 / pValor1;
         lEsperado := lEsperado + (pValor3 / pValor2);
         lObtido   := FTarefa3Controller.GetTotalDivisoes;
      end;
   end;

   if lEsperado <> lObtido then
   begin
      raise Exception.Create('Valor obtido difere do valor esperado');
   end;
end;


procedure TMyTestObject.TestSQLTarefa1SQLCompleto;
var
   lController: TTarefa1Controller;
   lColunas, lTabelas, lCondicoes: string;
begin
   lController := TTarefa1Controller.Create;
   try
      lColunas := 'coluna1 ' + sLineBreak + 'coluna2' ;
      lTabelas := 'tabela';
      lCondicoes := 'coluna2 = 100' + sLineBreak + 'upper(coluna10) = ''VALOR''';
      lController.GeraSQL(lColunas, lTabelas, lCondicoes);
   finally
      lController.Destroy;
   end;
end;


initialization
  TDUnitX.RegisterTestFixture(TMyTestObject);
end.
