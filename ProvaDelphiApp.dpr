program ProvaDelphiApp;

uses
  Vcl.Forms,
  ufPrincipal in 'forms\ufPrincipal.pas' {FrmPrincipal},
  ufTarefa1 in 'forms\ufTarefa1.pas' {fTarefa1},
  uFMaster in 'forms\Master\uFMaster.pas' {fMaster},
  uTarefa1Controller in 'controllers\uTarefa1Controller.pas',
  uPrincipalController in 'controllers\uPrincipalController.pas',
  ufTarefa2 in 'forms\ufTarefa2.pas' {fTarefa2},
  uTarefa2Controller in 'controllers\uTarefa2Controller.pas',
  uThdProgressA in 'classes\uThdProgressA.pas',
  ufTarefa3 in 'forms\ufTarefa3.pas' {fTarefa3},
  uTarefa3Controller in 'controllers\uTarefa3Controller.pas',
  uProjetoModel in 'models\uProjetoModel.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TFrmPrincipal, FrmPrincipal);
  Application.Run;
end.
